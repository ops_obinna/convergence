<section class="container">

<div class="row">
	<section class="footer-section footer-sec1 col-12 col-sm-6 col-md-4 col-lg-3">
		<a href=""><img src = "{{asset('img/logo.png')}}" title = "Convergence studio" style="height:50px; width: 50%;" alt = "Convergence studio"/></a>
	</section>

	<section class="footer-section col-12 col-sm-6 col-md-4 col-lg-3">
		<p class="links-heading">Convergence</p>
		<nav class="footer-list">
			<ul class="footer-links">
				<li>About Us</li>
				<li>Help</li>
				<li>For Landords</li>
				<li>For Business</li>
			</ul>
		</nav>
	</section>

	<section class="footer-section col-12 col-sm-6 col-md-4 col-lg-3">
		<p class="links-heading">Destinations</p>
		<nav>
			<ul class="footer-links">
				<li>Yaba</li>
				<li>Sururlere</li>
				<li>Victoria Island</li>
				<li>Lekki</li>
				<li>Ikorodu</li>
				<li>Ikeja</li>
				<li>Shomolu</li>
				<li>Aguda</li>
			</ul>
		</nav>
	</section>

	<section class="footer-section col-12 col-sm-6 col-md-4 col-lg-3">
		<p class="links-heading">Legal</p>
		<nav>
			<ul class="footer-links">
				<li>Terms and Conditions</li>
				<li>Impressum</li>
				<li>Privacy Policy</li>
			</ul>
		</nav>
	</section>
</div>
<hr style="background-color: white;">
<div class="footer-row row">
	<section class="footer-social col-12 col-sm-6 col-md-4 col-lg-4">
		<a class="social-icon" href=""><i class="fab fa-twitter"></i></a>
		<a class="social-icon" href=""><i class="fab fa-facebook"></i></a>
		<a class="social-icon" href=""><i class="fab fa-linkedin"></i></a>
		<a class="social-icon" href=""><i class="fab fa-instagram"></i></a>
	</section>
	<section class="footer-trademark col-12 col-sm-6 col-md-4 col-lg-4">
		<p>copyright &copy; 2019 Convergence Studios</p>
	</section>
	<section class="footer-country col-12 col-sm-6 col-md-4 col-lg-4">
		<a class="country-link" href=""><i class="fas fa-flag"></i></a>
		<p>Nigeria</p>
	</section>
</div>
	
</section>