<!doctype html>
<html lang="en">

<head>
<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Convergence') }}</title>

        <title>Covergence|Studios</title>

        <link rel="icon" type="image/png" href="{{ url('img/favicon.png') }} ">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        
        
        <script defer src="{{asset('js/lib/fontawesome-all.min.js')}}"></script>
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/animate.css')}}">
        <!-- Custom CSS -->
        <link href="{{asset('css/signupreg.css')}}" rel="stylesheet">

</head>

<body>


	 @yield('main')



   <script type="text/javascript">
            @if(env('APP_ENV') != 'local') {
              window.baseUrl = "{{secure_url('/')}}";
            @else
              window.baseUrl = "{{url('/')}}";
            @endif
      </script>
   
    <script src="{{asset('js/lib/jquery.min.js')}}"></script>
    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/lib/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('js/script.js')}}"></script>
  
</body>

</html>