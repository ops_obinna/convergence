<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no,minimum-scale=1, maximum-scale=1">

       
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Convergence') }}</title>

        <title>Covergence|Studios</title>

        <link rel="icon" type="image/png" href="{{ url('img/favicon.png') }} ">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <link href="https://fonts.googleapis.com/css?family=Arvo|Open+Sans&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        
        
        <script defer src="{{asset('js/lib/fontawesome-all.min.js')}}"></script>
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/animate.css')}}">
        <!-- Custom CSS -->
        <link href="{{asset('css/custom.css')}}" rel="stylesheet">

        <!-- Swiper CSS -->
        <link href="{{asset('css/swiper.min.css')}}" rel="stylesheet">

    </head>
    <body>
        
       <header id="page-hero" class="site-header"> @include('layouts.header')</header>

       @yield('content')



    
    <footer class="site-footer">@include('layouts.footer')</footer>

    
    <script src="{{asset('js/lib/jquery.min.js')}}"></script>
    <script src="{{asset('js/app.js')}}"></script>
    <script src="{{asset('js/lib/bootstrap.bundle.min.js')}}"></script>

    <script src="{{asset('js/script.js')}}"></script>

    <script src="{{asset('js/swiper.min.js')}}"></script>

      <!-- Initialize Swiper -->
  <script>
    var swiper = new Swiper('.swiper-container', {
      effect: 'coverflow',
      grabCursor: true,
      centeredSlides: true,
      slidesPerView: 'auto',
      coverflowEffect: {
        rotate: 50,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows : true,
      },
      pagination: {
        el: '.swiper-pagination',
      },
    });
  </script>
    
    </body>
</html>
