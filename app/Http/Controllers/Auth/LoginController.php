<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use App\User;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

        /**
     * Redirect the user to the Social Accounts authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Social Account.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $userSocial = Socialite::driver('facebook')->user();
        dd($userSocial);
        //check if user exist in DB 
        $findUserSocial = user::where('email', $userSocial->email)->first();

        if($findUserSocial){

            Auth::login($findUserSocial);

            return 'User found';

        }else{

            $user = new User;
            $user->name  = $userSocial->name;
            $user->username  = $userSocial->nickname;
            $user->email = $userSocial->email;
            $user->phone = $userSocial->phone;
            $user->password = bcrypt('1234');
            $user->save();
            Auth::login($user);

            return 'New User Aded';
        }
        
    }
}
