
@extends('layout')
@section('content')
<div class="site-header">
    <section class= "layout-hero">
          <div class="container">
            <h3 class="page-section-title">Rent Furnished Apartments</h3>
            <p class="page-section-text">Looking for a space - search no Further</p>

          <article class="search-box container">
          <section class="row">
            <form>
            <input class="search-field apartments" type="text" name="apartments" placeholder="Search Here for Available Apartments"/>

          

          <select class="search-field locations"  name="locations" placeholder="Pick a Location">
            <option value="volvo">Yaba</option>
            <option value="saab">Ikeja</option>
            <option value="mercedes">Surulere</option>
            <option value="audi">Ikorodu</option>
            <option value="audi">Victoria Island</option>
            <option value="audi">Lekki</option>
          </select>

          <button type="button" class="search-btn">Search</button>
            </form>
          </section>
          </article>

          </div>       
    </section> 
 </div>


 <article class="sky-bg">

   <section class="container "> 
       
     <div class="row d-flex justify-content-center">
      <section class="text-header family-sans col-12">
        Get Affordable Houses 
      </section>
       <section class="card-tel col-10 col-xs-10 col-sm-10 col-md-4">
        <a class="body-icon love-icon" href=""><i class="far fa-heart"></i></a>
         <div class="txt-head">
          <h5> High-quality furnished apartments </h5>
          <p class="text-body">Housing as a Service begins with the apartment. Our high quality standards include Wifi, washing machines and more for every home.</p>
         </div>
       </section>

       <section class="card-tel col-10 col-xs-10 col-sm-10 col-md-4">
          <a class="body-icon book-icon" href=""><i class=" fas fa-calendar-alt"></i></a>
         <div class="txt-head">
          <h5>Book easily</h5>
          <p class="text-body">You simply request an apartment, receive the confirmation and move in. Convergence ensures fair rental contracts and also offers viewings</p>
         </div>
       </section>
       
       <section class="card-tel col-10 col-xs-10 col-sm-10 col-md-4">
          <a class="body-icon star-rate" href=""><i class="far fa-star"></i></a>
         <div class="txt-head">
         <h5>Six Sigma level services</h5>
         <p class="text-body">Convergence has 5 stars on Tellvictor. If something goes wrong, our diligent service team is there for you and happy to assist.</p>
       </div>
       </section>
     </div>

   </section>
 
 </article>

<br>



 <!-- Swiper -->
 
<div class="slide-div container-fluid" >

        <!-- Swiper -->
  <div class="swiper-container">

  <section class="family-sans col-12 " style="text-align:center; font-size:2rem;">
    <p>FIND YOUR NEW HOME</p>
    </section>
  <p style="text-align:center;">Explore our shared housing locations and find your own space.</p>

    <div class="swiper-wrapper">
    <div class="swiper-slide col-xs-10 col-sm-10 col-md-3 col-lg-3">
        <section class="imgBx"><img class="img-slide" src = "{{asset('img/houses/1.png')}}" alt="" ></section>
        <section class="details"><h3>John Doe <br> <span>Web Designer</span></h3></section>
      </div>

      <div class="swiper-slide col-xs-10 col-sm-10 col-md-3 col-lg-3">
        <section class="imgBx"><img class="img-slide" src = "{{asset('img/houses/2.png')}}" alt="" ></section>
        <section class="details"><h3>Victoria Island <br> <span>Lagos</span></h3></section>
      </div>


      <div class="swiper-slide col-xs-10 col-sm-10 col-md-3 col-lg-3">
        <section class="imgBx"><img class="img-slide" src = "{{asset('img/houses/3.png')}}" alt="" ></section>
        <section class="details"><h3>Lekki <br> <span>Lagos</span></h3></section>
      </div>


      <div class="swiper-slide col-xs-10 col-sm-10 col-md-3 col-lg-3">
        <section class="imgBx"><img class="img-slide" src = "{{asset('img/houses/4.png')}}" alt="" ></section>
        <section class="details"><h3>surulere<br> <span>Lagos</span></h3></section>
      </div>


      <div class="swiper-slide col-xs-10 col-sm-10 col-md-3 col-lg-3">
        <section class="imgBx"><img class="img-slide" src = "{{asset('img/houses/1.png')}}" alt="" ></section>
        <section class="details"><h3>Ikeja<br> <span>Lagos</span></h3></section>
      </div>


      <div class="swiper-slide col-xs-10 col-sm-10 col-md-3 col-lg-3">
        <section class="imgBx"><img class="img-slide" src = "{{asset('img/houses/2.png')}}" alt="" ></section>
        <section class="details"><h3>Yaba <br> <span>Lagos</span></h3></section>
      </div>
      
    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination"></div>
  </div>


 

</div>
 
<!--Wnd Slider -->




 <article class="landing-subsection">
   <section class="container-fluid d-flex justify-content-center" >
     
      <div class="card d-flex justify-content-center" style="width:80rem;">
      <h3 class="text-h  famili-avo py-1">Relax, we are hospitable</h3>
        <h5 class="text-p family-sans"> Making you feel at home seems to be our hobby</h5>
          <section class="row"> 
              <section class="featured-attract col-12 col-xs-12 col-sm-12 col-md-6 col-lg-4"> 
                      <i class="fas fa-rss" style="font-size:50px;color:#FFC107"></i>
                      <p class="featured-head">Dedicated Wifi</p>
                        <p class="featured-body family-OpenSans">
                    Get your own dedicated WiFi router to ensure high speed downloads, seamless streaming, and constant connectivity.
                  </p>
              </section>

              <section class="featured-attract col-12 col-xs-12 col-sm-12 col-md-6 col-lg-4"> 
                  <i class="fas fa-desktop" style="font-size:50px;color:#FFC107"></i>
                  <p class="featured-head">TV, Cable & Netflixi</p>
                  <p class="featured-body family-OpenSans">
                    Every unit includes a TV and 12 months of Netflix on us. Oh, and did we mention you get premium cable?
                  </p>
              </section>
              <section class="featured-attract col-12 col-xs-12 col-sm-12 col-md-6 col-lg-4"> 
                <i class="material-icons" style="font-size:50px;color:#FFC107">pan_tool</i>
                  <p class="featured-head">Whiteglove Cleaning</p>
                   <p class="featured-body family-OpenSans">
                    Keep your place looking (and smelling) fresh. Our full-time Community concierge and housekeeping team clean your private space and common rooms weekly.
                  </p>
              </section>
              <section class="featured-attract col-12 col-xs-12 col-sm-12 col-md-6 col-lg-4"> 
              <i class="material-icons" style="font-size:50px;color:#FFC107">note</i>
                  <p class="featured-head">Simple Billing</p>
                   <p class="featured-body family-OpenSans">
                   We’ll cover all utilities like electricity, water, internet and more all in one simple bill. No unpleasant surprises here..
                  </p>
              </section>
              <section class="featured-attract col-12 col-xs-12 col-sm-12 col-md-6 col-lg-4"> 
                  <i class="fa fa-truck" style="font-size:50px;color:#FFC107"></i>
                  <p class="featured-head">Flexible Living</p>
                   <p class="featured-body family-OpenSans">
                    Want to move into a different vacant space? Give us a day’s notice, and we’ll help make it happen. Month-to-month agreement lets you live on your terms.
                  </p>
              </section>
              <section class="featured-attract col-12 col-xs-12 col-sm-12 col-md-6 col-lg-4"> 
                  <i class="material-icons" style="font-size:50px;color:#FFC107">local_hotel</i>
                  <p class="featured-head">Make it Your Own</p>
                   <p class="featured-body family-OpenSans">
                    Your place is your art. Decorate and completely design your suite how you want.
                  </p>
              </section>

              <section class="container" style="margin-bottom:5rem;">
                <p class="featured-body family-OpenSans">
                  From the moment you walk in the front door, you’ll feel the comfort and security that makes our residents happy to call us home. Cutting edge amenities, meticulously-groomed grounds, and a dedicated staff contributes to a higher standard of living. Convenient shopping, award-winning schools, local museums and parks are all close at hand, with sponsored activities to develop new hobbies while getting to know your neighbors.
                </p>
              </section>

          </section>

    </div>     
   </section>
 </article>



<div class="testimonial container-fluid"> 
<div class="container">
    <div class="row text-center family-sans">

      <section class="text-header family-sans col-12">
        What Customers are saying
      </section>

            <!--Grid column-->
          <div class="col-md-6">
            <div class="testimonial">
              <!--Avatar-->
              <div class="avatar mx-auto col-4 col-xs-4 col-sm-4 col-md-4  col-lg-4">
                <img src="{{asset('/img/c1.jpg')}}" class="rounded-circle img-fluid">
              </div>
              <!--Content-->
              <h4 class="font-weight-bold mt-4">John Barton</h4>
              <h6 class="blue-text font-weight-bold my-3">Student</h6>
              <p class="font-weight-normal"><i class="fas fa-quote-left pr-2"></i>Such nice and comfortable room at such 
                an affordable price that one can pay monthly is nothing but a miracle to me, I'll recommend you any day any time.
              </p>
              <!--Review-->
              <div class="grey-text">
                <i class="fas fa-star"> </i>
                <i class="fas fa-star"> </i>
                <i class="fas fa-star"> </i>
                <i class="fas fa-star"> </i>
                <i class="fas fa-star"> </i>
              </div>
            </div>
          </div> 


            <!--Grid column-->
          <div class="col-md-6">
            <div class="testimonial">
              <!--Avatar-->
              <div class="avatar mx-auto col-4 col-xs-4 col-sm-4 col-md-4  col-lg-4">
                <img src="{{asset('/img/c2.jpg')}}" class="rounded-circle img-fluid">
              </div>
              <!--Content-->
              <h4 class="font-weight-bold mt-4">Cornelius Ofobireke</h4>
              <h6 class="blue-text font-weight-bold my-3">Student</h6>
              <p class="font-weight-normal"><i class="fas fa-quote-left pr-2"></i>Accomodation in Lagos is not a joking matter ooo, but convergence studios  have done the assignment perfectly and give the result to me 
              at a very reasonable price, thank you.</p>
              <!--Review-->
              <div class="grey-text">
                <i class="fas fa-star"> </i>
                <i class="fas fa-star"> </i>
                <i class="fas fa-star"> </i>
                <i class="fas fa-star"> </i>
               </div>
            </div>
          </div>     
  </div>
</div>
</div>

<div class="container-fluid d-flex justify-content-center" style="background-color:#a79a2e; height:30vh;color:white;">
  <section class="row">
    <article class="col-12 justify-content-center" style="margin-top:5rem;">
    <section class="family-sans col-12" style="text-align:center; font-size:2rem;">
      READY TO BECOME A MEMBER?
    </section>
    <section class="d-flex justify-content-center">
      <a href="#" class="button" style="padding:15px 35px; margin:0px 0px 16px; color:#883BA1; background:white; text-decoration:none;">Explore Locations</a>
    </section>
    </article>
  </section>
</div>
@endsection

 